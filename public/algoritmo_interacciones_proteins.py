import json
import sys
graph = json.load(open('grafo_interacionts_proteins_5000.json', 'r'))
graph = {int(k):v for k,v in graph.items()}
identifierProteins = json.load(open('identifiers_proteins_5000.json', 'r'))

def query(proteins, min_ppi):
	N = len(graph)
	count = [0 for _ in range(N+1)]
	for protein in proteins:
		count[protein] = N
		for neighbor in graph[protein]:
			if protein == neighbor and min_ppi != 1:
				continue
			count[neighbor] += 1
	
	result = {}
	
	for x in range(1,N+1):
		result[x] = []

	for protein in proteins:
		for neighbor in graph[protein]:
			if protein == neighbor and min_ppi != 1:
				continue
			if count[protein] >= min_ppi and count[neighbor] >= min_ppi:
				result[protein].append(neighbor)

	result_aux = {}
	for k,v in result.items():
		if len(v) != 0:
			result_aux[k] = v

	return result_aux

if __name__ == "__main__":
	#n,min_ppi = map(int, [int(x) for x in input().split()])
	
	l = [int (x) for x in sys.argv[1:-1]]
	min_ppi = int(sys.argv[-1])
	
	#print(l,min_ppi)
	
	rpta_aux = query(l,min_ppi)
	#print(rpta_aux)
	register_node = {}
	nodes = []
	links = []
	for k,v in rpta_aux.items():

		for target in v:
			aux_link = {
				"source": k,
				"target": target
			}
			links.append(aux_link)
			
			if register_node.get(target,None):
				if target != k :
					register_node[target] += 1
			else:
				register_node[target] = 1
				aux_node = {
					"id": target,
					"id_entrez" :  identifierProteins[str(target)]["id_entrez"],
					"id_uniprot" :  identifierProteins[str(target)]["id_uniprot"],
					"gene_symbol" :  identifierProteins[str(target)]["gene_symbol"],
					"cantPPI" : 0
				}
				if target in l:
					aux_node["color"] = "red"
					aux_node["cantPPI"] = len(rpta_aux[target])
				nodes.append(aux_node)


		if not register_node.get(k,None): 
			register_node[k] = len(v)
			aux_node = {
				"id": k,
				"id_entrez" :  identifierProteins[str(k)]["id_entrez"],
				"id_uniprot" :  identifierProteins[str(k)]["id_uniprot"],
				"gene_symbol" :  identifierProteins[str(k)]["gene_symbol"],
				"cantPPI" : len(v),
				"color" : "red"
			}
			nodes.append(aux_node)
	for node in nodes:
		if node['cantPPI'] == 0:
			node['cantPPI'] = register_node[node['id']]

	rpta_final = {'nodes': nodes, 'links': links}

	rpta_final = str(rpta_final)
	rpta_final = rpta_final.replace("\'","\"")
	print(rpta_final, end="")
