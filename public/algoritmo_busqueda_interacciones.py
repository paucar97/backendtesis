import sys
import json
with open('grafo_interacionts_proteins_5000.json') as json_file:
    grafoAux = json.load(json_file)
with open('identifiers_proteins_5000.json') as json_file:
    identifierProteins = json.load(json_file)

grafo = {int(k):v for k,v in grafoAux.items()}
#print(grafo)
#grafo = {
#    1: [2,3,4,11],
#    2: [1,5,6],
#    3: [1,10],
#    4: [1,7],
#    5: [2],
#    6: [2],
#    7: [4,9,8],
#    8: [9,7],
#    9: [15,8,7],
#    10: [3,15],
#    11: [1,15],
#    12: [15],
#    13: [14],
#    14: [13],
#    15: [12,11,10,9]
#}

resultado = {}

""" Definiciones generales """
N = len(grafo) # Numero de nodos
INF = int(1e9)

""" Definiciones para la solución """
cancelado = [False for _ in range(N+1)]
directo = [False for _ in range(N+1)]
es_solucion = [False for _ in range(N+1)]
tam_camino = [0 for _ in range(N+1)]

#########################################################
""" Arreglos para obtener puentes """
low =  [0 for _ in range(N + 1)]
ids =  [-1 for _ in range(N + 1)]
parent = [0 for _ in range(N + 1)]
cur_id = [0]
grafo_puentes = {}

def dfs_puentes(u):
	low[u] = ids[u] = cur_id[0]
	cur_id[0] += 1
	for v in grafo[u]:
		if ids[v] == -1:
			parent[v] = u
			dfs_puentes(v)
			if low[v] > ids[u]:
				grafo_puentes[v].append(u)
				grafo_puentes[u].append(v)
			
			low[u] = min(low[u], low[v])
		elif v != parent[u]:
			low[u] = min(low[u], ids[v])

def obtener_puentes():
	# Inicializando grafo de puentes
	for nodo in range(1, N + 1):
		grafo_puentes[nodo] = []
	
	for nodo in range(1, N + 1):
		if ids[nodo] == -1:
			dfs_puentes(nodo)
##########################################################

##########################################################
def dfs_cancelar(u, padre):
	cancelado[u] = True
	for v in grafo[u]:
		if v == padre:
			continue

		if not cancelado[v]:
			dfs_cancelar(v, u)
###########################################################

###########################################################
import queue

def bfs(s, su, sv):
	if su > sv:
		tmp = su
		su = sv
		sv = tmp

	dist = [INF for _ in range(N + 1)]
	
	q = queue.Queue()
	dist[s] = 0
	q.put(s)

	while not q.empty():
		u = q.get()
		for v in grafo[u]:
			if dist[v] > dist[u] + 1:
				ru, rv = u, v
				if ru > rv:
					tmp = ru
					ru = rv
					rv = tmp

				if ru == su and rv == sv:
					continue

				dist[v] = dist[u] + 1
				q.put(v)

	return dist


vis_u = [False for _ in range(N+1)]
vis_v = [False for _ in range(N+1)]

def valid(u):
	return vis_u[u] and vis_v[u]

def dfs_fix_v(u, pai):
	vis_v[u] = True
	for v in resultado[u]:
		if v == pai:
			continue
		if vis_v[v]:
			continue

		dfs_fix_v(v,u)

def dfs_fix_u(u, pai):
	vis_u[u] = True
	for v in resultado[u]:
		if v == pai:
			continue
		if vis_u[v]:
			continue

		dfs_fix_u(v,u)

def fix_pacu(graph, u, v):
	dfs_fix_u(u, v)
	dfs_fix_v(v, u)

	result = {}
	for node in range(1,N+1):
		result[node] = []
	
	for node in range(1,N+1):
		for vec in resultado[node]:
			if valid(node) and valid(vec):
				result[node].append(vec)

	return result


def find_graph(u, v, d):
	"""
		u, v: Nodos azules
		d: PPI (distancia minima)
	"""
	global resultado

	#Obtener puentes del grafo
	obtener_puentes()


	#Guardar nodos a los que se llegan directamente por medio de un puente
	for vecino in grafo_puentes[u]:
		if vecino == v:
			continue
		directo[vecino] = True
		dfs_cancelar(vecino, u)
		cancelado[vecino] = False

	for vecino in grafo_puentes[v]:
		if vecino == u:
			continue
		directo[vecino] = True
		dfs_cancelar(vecino, v)
		cancelado[vecino] = False

	dist_u = bfs(u,u,v)
	dist_v = bfs(v,u,v)

		  
	for nodo in range(1, N + 1):
		resultado[nodo] = []

#	if dist_u[v] == INF: #Cuando u y v estan desconectados
#		return resultado
	
	#Caso general
	for nodo in range(1, N + 1):
		if cancelado[nodo] or directo[nodo] or dist_u[nodo] == INF:
			tam_camino[nodo] = -1
			continue
		
		tam_camino[nodo] = dist_u[nodo] + dist_v[nodo]
	
	for nodo in range(1, N + 1):
		if tam_camino[nodo] == -1 or (nodo != u and nodo != v and tam_camino[nodo] < d):
			continue

		for vecino in grafo[nodo]:
			if tam_camino[vecino] == -1 or (vecino != u and vecino != v and tam_camino[vecino] < d):
				continue
			
			if nodo == u or nodo == v or vecino == u or vecino == v or tam_camino[nodo] == tam_camino[vecino]:
				resultado[nodo].append(vecino)
			
	if d == 1:
		for vecino in grafo[u]:
			if directo[vecino]:
				resultado[u].append(vecino)
				resultado[vecino].append(u)

		for vecino in grafo[v]:
			if directo[vecino]:
				resultado[v].append(vecino)
				resultado[vecino].append(v)
	
	new_resultado = fix_pacu(resultado, u, v)
	
	return new_resultado

#u, v, d = map(int, [int(x) for x in input().split()])
u, v, d = sys.argv[1:]

#print(u, v, d)
u, v, d = int(u),int(v),int(d)
ini = u
fin = v
#print(u, v, d)
rpta = find_graph(u, v, d)

rpta_aux = {}
for k,v in rpta.items():
	if len(v) !=0:
		rpta_aux[k] = v
#print(rpta_aux)
nodes = []
links = []
for k,v in rpta_aux.items():
	aux_node = {
		"id": k,
		"test": f"Nodo {k}",
		#"identifiersProteins" : identifierProteins[str(k)] 
		"id_entrez" :  identifierProteins[str(k)]["id_entrez"],
		"id_uniprot" :  identifierProteins[str(k)]["id_uniprot"],
		"gene_symbol" :  identifierProteins[str(k)]["gene_symbol"],
		"cantPPI" : len(v)
	}
	if k == ini or k == fin:
		aux_node["color"] = "red"
		
	nodes.append(aux_node)
	for target in v:
		aux_link = {
			"source": k,
			"target": target
		}
		links.append(aux_link)
graphData = {"nodes":nodes,"links":links}
graphData = str(graphData)
graphData = graphData.replace("\'","\"")
print(graphData, end="")