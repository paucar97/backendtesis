<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SequenceInfo extends Model
{
    protected $table = "db_sequence_info";
    protected $fillable = ['gene_symbol','id_protein','id_uniprot_type'];
    protected $primaryKey='id_uniprot';
    protected $keyType = 'string';
    public $timestamps = false;

    public function uniprotType()
    {
        return $this->belongsTo('App\UniprotType','id_uniprot_type');
    }

    public function protein()
    {
        return $this->belongsTo('App\Protein','id_protein');
    }
}
