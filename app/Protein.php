<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Protein extends Model
{
    protected $table = "db_protein";
    protected $fillable = ['protein_name', 'id_organism'];
    protected $primaryKey='id_protein';

    public $timestamps = false;

    public function entrezes()
    {
        return $this->hasMany('App\Entrez','id_protein');
    }

    public function sequencesInfo()
    {
        return $this->hasMany('App\SequenceInfo','id_protein');
    }

    public function interactionProteins1()
    {
        return $this->hasMany('App\InteractionProtein','id_protein1');
    }

    public function interactionProteins2()
    {
        return $this->hasMany('App\InteractionProtein','id_protein2');
    }

    public function organism()
    {
        return $this->belongsTo('App\Organism','id_organism');
    }
}
