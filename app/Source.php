<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Source extends Model
{
    protected $table = "db_source";
    protected $fillable = ['name'];
    protected $primaryKey='id_source';

    public $timestamps = false;

    public function interactionProteins()
    {
        return $this->belongsToMany('App\InteractionProtein','db_interaction_x_source','id_source','id_interaction_protein');
    }
}
