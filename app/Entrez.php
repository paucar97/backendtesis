<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entrez extends Model
{
    protected $table = "db_entrez";
    protected $fillable = ['entrez_id','id_protein'];
    protected $primaryKey='id_entrez';

    public $timestamps = false;

    public function protein()
    {
        return $this->belongsTo('App\Protein','id_protein');
    }
}
