<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organism extends Model
{
    protected $table = "db_organism";
    protected $fillable = ['name'];
    protected $primaryKey='id_organism';

    public $timestamps = false;

    public function proteins()
    {
        return $this->hasMany('App\Protein','id_organism');
    }
}
