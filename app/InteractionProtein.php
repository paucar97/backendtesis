<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InteractionProtein extends Model
{
    protected $table = "db_interaction_protein";
    protected $fillable = ['score','id_protein1','id_protein2'];
    protected $primaryKey='id_interaction_protein';

    public $timestamps = false;

    public function protein1()
    {
        return $this->belongsTo('App\Protein','id_protein1');
    }

    public function protein2()
    {
        return $this->belongsTo('App\Protein','id_protein2');
    }

    public function sources()
    {
        return $this->belongsToMany('App\Source','db_interaction_x_source','id_interaction_protein','id_source');
    }
}
