<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UniprotType extends Model
{
    protected $table = "db_uniprot_type";
    protected $fillable = ['name'];
    protected $primaryKey='id_uniprot_type';

    public $timestamps = false;

    public function sequencesInfo()
    {
        return $this->hasMany('App\SequenceInfo','id_uniprot_type');
    }
}
