<?php

namespace App\Http\Controllers;

use App\UniprotType;
use Illuminate\Http\Request;

class UniprotTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UniprotType  $uniprotType
     * @return \Illuminate\Http\Response
     */
    public function show(UniprotType $uniprotType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UniprotType  $uniprotType
     * @return \Illuminate\Http\Response
     */
    public function edit(UniprotType $uniprotType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UniprotType  $uniprotType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UniprotType $uniprotType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UniprotType  $uniprotType
     * @return \Illuminate\Http\Response
     */
    public function destroy(UniprotType $uniprotType)
    {
        //
    }
}
