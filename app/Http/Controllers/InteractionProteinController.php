<?php

namespace App\Http\Controllers;

use App\InteractionProtein;
use App\Entrez;
use App\SequenceInfo;
use App\Http\Controllers\ProteinController;
use Illuminate\Http\Request;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class InteractionProteinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InteractionProtein  $interactionProtein
     * @return \Illuminate\Http\Response
     */
    public function show(InteractionProtein $interactionProtein,$id)
    {
        //
        
        $interactionsProteinList = InteractionProtein::where('id_protein1','=',$id)->with('protein2.sequencesInfo','protein2.entrezes')->get();
        //$interactionsProteinList = InteractionProtein::find(1);
        return ["data" => $interactionsProteinList];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InteractionProtein  $interactionProtein
     * @return \Illuminate\Http\Response
     */
    public function edit(InteractionProtein $interactionProtein)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InteractionProtein  $interactionProtein
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InteractionProtein $interactionProtein)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InteractionProtein  $interactionProtein
     * @return \Illuminate\Http\Response
     */
    public function destroy(InteractionProtein $interactionProtein)
    {
        //
    }

    private function get_id_system($proteinUnknownIdentifier){
        $entrezId = ctype_digit($proteinUnknownIdentifier)? intval($proteinUnknownIdentifier) : null;
        if ($entrezId === null){// no es entrez id
            $stringHuman = '_HUMAN';
            $proteinUnknownIdentifier = strtoupper($proteinUnknownIdentifier);
            $pos = strpos($proteinUnknownIdentifier, $stringHuman);
            if ($pos === false){
                // Es gene symbol
                $geneSymbol = $proteinUnknownIdentifier; // aqui es gene symbol
                $idProtein = SequenceInfo::select('id_protein')->where('gene_symbol', '=',$geneSymbol )->get();
            }else{
                $idUniprot = $proteinUnknownIdentifier; // aqui es id uniprot
                $idProtein = SequenceInfo::select('id_protein')->where('id_uniprot', '=',$idUniprot )->get();
            }
        }else{
            $idProtein = Entrez::select('id_protein')->where('entrez_id','=',$entrezId)->get();
            
        }
        $idProtein = $idProtein[0]["id_protein"];
        return $idProtein;
    }
    public function show_interactions($listUnknownIdenfierProtein,$dFactor)
    {
        $listCleany = explode(";",$listUnknownIdenfierProtein);
        $listCleany = array_filter($listCleany);
        $listaProteins =  array();
        foreach ($listCleany as &$tuplaProtein){
            list($p1, $p2) = explode(" ",$tuplaProtein);
            if ($p1 == $p2)
                array_push($listaProteins,$p1);
            else
                array_push($listaProteins,$p1, $p2 );
        }
        $lineCommand = "python3 algoritmo_interacciones_proteins.py";
        foreach ($listaProteins as &$protein){
            $id = $this->get_id_system($protein);
            $lineCommand = $lineCommand . " " . $id;
        }
        $lineCommand = $lineCommand . " " . $dFactor . " 2>&1";
        $output = shell_exec($lineCommand);
        $data =json_decode($output,true);
        return ["data" => $data ];
    }
    public function interactions_proteins($listUnknownIdenfierProtein,$dFactor)
    {
        //$output = str_replace("\\","", $output);   
        // Consider only 2 proteins
        $listCleany = explode(";",$listUnknownIdenfierProtein);
        $listCleany = array_filter($listCleany);
        $TwoProteins = explode(" ",$listCleany[0]);
        $idProtein1 = $this->get_id_system($TwoProteins[0]);
        $idProtein2 = $this->get_id_system($TwoProteins[1]);

        $command = escapeshellcmd('python test.py');
        $lineCommand =  "python3 algoritmo_busqueda_interacciones.py " . $idProtein1 . " ". $idProtein2 ." ". $dFactor ." 2>&1";
        $output = shell_exec($lineCommand);
        $data =json_decode($output,true);
        return ["data" => $data ];

    }
}
