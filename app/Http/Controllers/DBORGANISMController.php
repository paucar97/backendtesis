<?php

namespace App\Http\Controllers;

use App\DB_ORGANISM;
use Illuminate\Http\Request;

class DBORGANISMController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DB_ORGANISM  $dB_ORGANISM
     * @return \Illuminate\Http\Response
     */
    public function show(DB_ORGANISM $dB_ORGANISM)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DB_ORGANISM  $dB_ORGANISM
     * @return \Illuminate\Http\Response
     */
    public function edit(DB_ORGANISM $dB_ORGANISM)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DB_ORGANISM  $dB_ORGANISM
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DB_ORGANISM $dB_ORGANISM)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DB_ORGANISM  $dB_ORGANISM
     * @return \Illuminate\Http\Response
     */
    public function destroy(DB_ORGANISM $dB_ORGANISM)
    {
        //
    }
}
