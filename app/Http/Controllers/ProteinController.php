<?php

namespace App\Http\Controllers;

use App\Protein;
use App\Entrez;
use App\SequenceInfo;
use Illuminate\Http\Request;

class ProteinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $proteins = Protein::with(['sequencesInfo','entrezes'])->get();
        //$proteins = Protein::with(['sequencesInfo','entrezes'])->paginate(10);
        return ["data" => $proteins];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Protein  $protein
     * @return \Illuminate\Http\Response
     */
    public function show(Protein $protein)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Protein  $protein
     * @return \Illuminate\Http\Response
     */
    public function edit(Protein $protein)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Protein  $protein
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Protein $protein)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Protein  $protein
     * @return \Illuminate\Http\Response
     */
    public function destroy(Protein $protein)
    {
        //
    }

    public function get_sequence($entrezId){
        $idProtein = Entrez::select('id_protein')->where('entrez_id','=',$entrezId)->get();
        $SequenceInfo = SequenceInfo::select('id_uniprot','gene_symbol')->where('id_protein','=',$idProtein[0]["id_protein"])->get();
        
        return ["data"=>$SequenceInfo[0]];
    }
    public function get_identifiers($proteinUnknownIdentifier){
        $entrezId = ctype_digit($proteinUnknownIdentifier)? intval($proteinUnknownIdentifier) : null;
        if ($entrezId === null){// no es entrez id
            $stringHuman = '_HUMAN';
            $proteinUnknownIdentifier = strtoupper($proteinUnknownIdentifier);
            $pos = strpos($proteinUnknownIdentifier, $stringHuman);
            if ($pos === false){
                // Es gene symbol
                $geneSymbol = $proteinUnknownIdentifier; // aqui es gene symbol
                $idProtein = SequenceInfo::select('id_protein')->where('gene_symbol', '=',$geneSymbol )->get();
            }else{
                $idUniprot = $proteinUnknownIdentifier; // aqui es id uniprot
                $idProtein = SequenceInfo::select('id_protein')->where('id_uniprot', '=',$idUniprot )->get();
            }
        }else{
            $idProtein = Entrez::select('id_protein')->where('entrez_id','=',$entrezId)->get();
            
        }
        $idProtein = $idProtein[0]["id_protein"];
        $proteinInfo = Protein::with(['sequencesInfo','entrezes'])->where('id_protein','=',$idProtein)->get();
        
        $rpta = [
            "id" => $idProtein,
            "id_entrez" => $proteinInfo[0]["entrezes"][0]['entrez_id'],
            "id_uniprot" =>$proteinInfo[0]["sequencesInfo"][0]["id_uniprot"],
            "gene_symbol" => $proteinInfo[0]["sequencesInfo"][0]["gene_symbol"]
        ];
        return ["data"=>$rpta ];
    }
}
