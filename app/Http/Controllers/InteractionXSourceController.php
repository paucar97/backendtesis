<?php

namespace App\Http\Controllers;

use App\InteractionXSource;
use Illuminate\Http\Request;

class InteractionXSourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InteractionXSource  $interactionXSource
     * @return \Illuminate\Http\Response
     */
    public function show(InteractionXSource $interactionXSource)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InteractionXSource  $interactionXSource
     * @return \Illuminate\Http\Response
     */
    public function edit(InteractionXSource $interactionXSource)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InteractionXSource  $interactionXSource
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InteractionXSource $interactionXSource)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InteractionXSource  $interactionXSource
     * @return \Illuminate\Http\Response
     */
    public function destroy(InteractionXSource $interactionXSource)
    {
        //
    }
}
