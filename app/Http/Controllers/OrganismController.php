<?php

namespace App\Http\Controllers;

use App\Organism;
use Illuminate\Http\Request;

class OrganismController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $organism = Organism::all();
        return ["data" => $organism];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Organism  $organism
     * @return \Illuminate\Http\Response
     */
    public function show(Organism $organism)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Organism  $organism
     * @return \Illuminate\Http\Response
     */
    public function edit(Organism $organism)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Organism  $organism
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Organism $organism)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Organism  $organism
     * @return \Illuminate\Http\Response
     */
    public function destroy(Organism $organism)
    {
        //
    }
}
