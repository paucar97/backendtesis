<?php

namespace App\Http\Controllers;

use App\SequenceInfo;
use Illuminate\Http\Request;

class SequenceInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SequenceInfo  $sequenceInfo
     * @return \Illuminate\Http\Response
     */
    public function show(SequenceInfo $sequenceInfo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SequenceInfo  $sequenceInfo
     * @return \Illuminate\Http\Response
     */
    public function edit(SequenceInfo $sequenceInfo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SequenceInfo  $sequenceInfo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SequenceInfo $sequenceInfo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SequenceInfo  $sequenceInfo
     * @return \Illuminate\Http\Response
     */
    public function destroy(SequenceInfo $sequenceInfo)
    {
        //
    }
}
