<?php

namespace App\Http\Controllers;

use App\Entrez;
use Illuminate\Http\Request;

class EntrezController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entrez  $entrez
     * @return \Illuminate\Http\Response
     */
    public function show(Entrez $entrez)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entrez  $entrez
     * @return \Illuminate\Http\Response
     */
    public function edit(Entrez $entrez)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entrez  $entrez
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Entrez $entrez)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entrez  $entrez
     * @return \Illuminate\Http\Response
     */
    public function destroy(Entrez $entrez)
    {
        //
    }
}
