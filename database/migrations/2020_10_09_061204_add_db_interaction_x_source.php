<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDbInteractionXSource extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('db_interaction_x_source', function (Blueprint $table) {
            $table->increments('id_interaction_x_source');
            $table->integer('id_interaction_protein')->unsigned();
            $table->integer('id_source')->unsigned();

            $table->foreign('id_interaction_protein')->references('id_interaction_protein')->on('db_interaction_protein');
            $table->foreign('id_source')->references('id_source')->on('db_source');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('db_interaction_x_source');
    }
}
