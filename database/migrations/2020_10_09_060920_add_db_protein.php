<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDbProtein extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('db_protein', function (Blueprint $table) {
            $table->increments('id_protein');
            $table->string('protein_name',500);
            $table->integer('id_organism')->unsigned();

            $table->foreign('id_organism')->references('id_organism')->on('db_organism');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('db_protein');
    }
}
