<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDbInteractionProtein extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('db_interaction_protein', function (Blueprint $table) {
            $table->increments('id_interaction_protein');
            $table->double('score');
            $table->integer('id_protein1')->unsigned();
            $table->integer('id_protein2')->unsigned();

            $table->foreign('id_protein1')->references('id_protein')->on('db_protein');
            $table->foreign('id_protein2')->references('id_protein')->on('db_protein');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('db_interaction_protein');
    }
}
