<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDbSequenceInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('db_sequence_info', function (Blueprint $table) {
            $table->string('id_uniprot',45)->primary();
            $table->string('gene_symbol',45);
            $table->integer('id_protein')->unsigned();
            $table->integer('id_uniprot_type')->unsigned();

            $table->foreign('id_protein')->references('id_protein')->on('db_protein');
            $table->foreign('id_uniprot_type')->references('id_uniprot_type')->on('db_uniprot_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('db_sequence_info');
    }
}
