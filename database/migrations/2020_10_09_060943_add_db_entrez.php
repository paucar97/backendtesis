<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDbEntrez extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('db_entrez', function (Blueprint $table) {
            $table->increments('id_entrez');
            $table->string('entrez_id',45);
            $table->integer('id_protein')->unsigned();

            $table->foreign('id_protein')->references('id_protein')->on('db_protein');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('db_entrez');
    }
}
