<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::apiResource("organism","OrganismController");
Route::apiResource("protein","ProteinController");
Route::apiResource("interactionsProtein","InteractionProteinController");
Route::get("show_interactions/{listUnknownIdenfierProtein}/{dFactor}","InteractionProteinController@show_interactions");
Route::get("indirect_interactions/{listUnknownIdenfierProtein}/{dFactor}","InteractionProteinController@interactions_proteins");
Route::get("get_sequence_info/{entrezId}","ProteinController@get_sequence");
Route::get("get_identifiers/{proteinUnknownIdentifier}","ProteinController@get_identifiers");